

# BUILD

```bash
cd electron-1.8.2~beta.3
debuild -S
```

# UPLOAD

```bash
dput ppa:yourlaunchpadid/yourppa *.changes
```


# The bin directory

This directory contains prebuild electron distribution. There must be one and only one zip file for any architecture. Only one version may reside in this folder.

To be precise, there must be one and only one zip file that matches each of the patterns below (one zip file for each pattern):

1. `*ia32*`
2. `*x64*`
3. `*armv7*`
4. `*arm64*` (>= electron 1.8.2-b3)
