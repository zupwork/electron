#!/bin/bash
dst=$1
DEB_HOST_ARCH=$2
if [[ "$DEB_HOST_ARCH" = amd64 ]]; then 
    bin_zip=`find bin -type f -name '*x64*'`;
    echo "Installing for amd64"
elif [[ "$DEB_HOST_ARCH" = i386 ]] || [[ "$DEB_HOST_ARCH" = i686 ]] || [[ "$DEB_HOST_ARCH" = ia32 ]]; then 
    bin_zip=`find bin -type f -name '*ia32*'`;
    echo "Installing for i386"
elif [[ "$DEB_HOST_ARCH" = armhf ]]; then
    bin_zip=`find bin -type f -name '*armv7*'`;
    echo "Installing for armhf"
elif [[ "$DEB_HOST_ARCH" = armel ]]; then
    bin_zip=`find bin -type f -name '*armv7*'`;
    echo "Installing for armel"
elif [[ "$DEB_HOST_ARCH" = arm64 ]]; then
    bin_zip=`find bin -type f -name '*arm64*'`;
    echo "Installing for arm64"
else 
    echo "Unsupported architecture: $DEB_HOST_ARCH" >&2; 
    exit 1; 
fi
unzip -ud "$dst" "$bin_zip"
